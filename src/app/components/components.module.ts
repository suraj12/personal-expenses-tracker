import { IonicModule } from '@ionic/angular';
import { AddExpenseComponent } from './add-expense/add-expense.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BargraphComponent } from './bargraph/bargraph.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [ BargraphComponent, AddExpenseComponent],
    exports: [BargraphComponent, AddExpenseComponent],
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule
    ]
})

export class ComponentModule {}