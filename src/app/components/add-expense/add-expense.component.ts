import { DatetimeService } from './../../services/datetime/datetime.service';
import { ExpenseInterface } from './../../interface/expenseinterface';
import { ActionService } from './../../services/action/action.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.scss'],
})
export class AddExpenseComponent implements OnInit {

  expenseForm: ExpenseInterface;

  addExpenseForm = new FormGroup({
    amount: new FormControl('', Validators.required),
    description: new FormControl(''),
    type: new FormControl('', Validators.required),
  });

  constructor(
    private modalController: ModalController, 
    private actionService: ActionService,
    private dateTimeService: DatetimeService
    ) {
    //  this.actionService.testFunc();
   }

  ngOnInit() {
    //console.log(this.addExpenseForm.value);
  }

  initCreateExpense(): void {
    const expense = this.addExpenseForm.value;
    // expense.createdOn = this.dateTimeService.getCurrentDateTime();
    this.dateTimeService.getSelectedDate()
    .then((date: Date) => {
      if(!expense.createdOn) {
        //expense.createdOn = this.dateTimeService.getCurrentDateTime();
        expense.createdOn = date;
      }
     // console.log(expense.createdOn);
    }).then(() => {
      this.actionService.createExpense(expense).then(() => {
        console.log('Expense was created');
        this.dismisModal();
      }).catch((err) => console.log(err));
    });
        
    
  }

  dismisModal(): void {
    this.modalController.dismiss().then().catch();
  }
}
