import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Chart } from "chart.js";

@Component({
  selector: 'app-bargraph',
  templateUrl: './bargraph.component.html',
  styleUrls: ['./bargraph.component.scss'],
})
export class BargraphComponent implements OnInit {

  @ViewChild("barCanvas") barCanvas: ElementRef;

  private barChart: Chart;

  constructor() { }

  ngOnInit() {}

  ngAfterViewInit() {

    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",
      data: {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thus", "Fri", "Sat"],
        datasets: [
          {
            label: "Expenses",
            data: [12, 19, 3, 5, 2, 3, 7],
            backgroundColor: 'rgb(38, 194, 129)' ,
            borderColor: 'rgb(38, 194, 129)',
            borderWidth: 1
          }
        ]
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      }
    });


  }

}
