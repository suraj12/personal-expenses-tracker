import { DataService } from './../data/data.service';
import { ExpenseInterface } from './../../interface/expenseinterface';
import { DatetimeService } from './../datetime/datetime.service';
import { Plugins } from '@capacitor/core';
import { Injectable } from '@angular/core';

// const { Storage } = Plugins;
 
@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(
    private dateTimeService: DatetimeService,
    private dataService: DataService
  ) { }

  async saveExpenseToLocal(expense: ExpenseInterface): Promise<void> {
    //const key = this.dateTimeService.getDateTimeISO();
    const key = this.dateTimeService.getDateTimeISO(expense.createdOn);
    //console.log(key);
    let expensesList: ExpenseInterface[] = [];
    return this.getFromLocalStorage(key).then((expenses) => {
     if (expenses == null) {
      expensesList.push(expense);
     } else {
      expensesList = expenses;
      expensesList.push(expense);
     }
    }).then(() => {
      this.saveToLocalStorage(key, expensesList).then(() => {
        this.dataService.setExpenses(expensesList);
      });
    }).catch((err) => console.log(err));
   
  }



  async getExpensesFromLocal(date?: Date): Promise<ExpenseInterface[]> {
    const key = date ? this.dateTimeService.getDateTimeISO(date) : this.dateTimeService.getDateTimeISO();
    return await this.getFromLocalStorage(key).then((expenses: ExpenseInterface[]) => {
        return expenses;
    });
}



  // JSON "set" 
  async saveToLocalStorage(key: string, value: any): Promise<void> {
    return await Plugins.Storage.set({
        key,
        value: JSON.stringify(value)
    });
}

  // JSON "get" 
  async getFromLocalStorage(key: string): Promise<{ value: any }> {
    const ret = await Plugins.Storage.get({key});
    return JSON.parse(ret.value);
  }

  // async setItem() {
  //   await Storage.set({
  //     key: 'name',
  //     value: 'Max'
  //   });
  // }
  
  // async getItem() {
  //   const { value } = await Storage.get({ key: 'name' });
  //   console.log('Got item: ', value);
  // }
  
  async removeFromLocalStorage(key: string): Promise<void> {
    await Plugins.Storage.remove({key});
  }
  
  // async keys() {
  //   const { keys } = await Storage.keys();
  //   console.log('Got keys: ', keys);
  // }
  
  async clearLocalStorage() {
    await Plugins.Storage.clear();
  }

}
