import { ExpenseInterface } from './../../../../../New folder/expensetracker/src/app/interface/expenseInterface';
import { DatetimeService } from './../datetime/datetime.service';
import { StorageService } from './../storage/storage.service';
import { async } from '@angular/core/testing';
import { ExpenseInterface } from './../../interface/expenseinterface';
import { DataService } from './../data/data.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActionService {

  demoExpense: ExpenseInterface;

  constructor(
    private dataService: DataService,
    private storageService: StorageService,
    private dateTimeService: DatetimeService
  ) { 
      // this.demoExpense = {
      //   amount: 50,
      //   description: 'this is demo expense',
      //   type: 'general',
      //   createdOn: new Date()
      // };
      // this.testFunc();
  }
  // testFunc() {
  //   this.dataService.setExpenses([this.demoExpense]).then(() => {
  //     this.dataService.getExpenses().then((val: ExpenseInterface[]) => {
  //       console.log(val);
  //     })
  //   });
  // }

  async createExpense(expense: ExpenseInterface): Promise<void> {
    // const key = this.dateTimeService.getDateTimeISO(expense.createdOn);
    //this.storageService.saveToLocalStorage(key, expense);
    return await this.storageService.saveExpenseToLocal(expense).then().catch();
  }

  async getTodaysExpensesFromLocal(): Promise<ExpenseInterface[]> {
    return await this.storageService.getExpensesFromLocal().then((expenses: ExpenseInterface[]) => {
      return expenses;
    });
  }

  async emitExpensesByDateFromLocal(date: Date): Promise<void> {
    return await this.storageService.getExpensesFromLocal(date).then((expenses: ExpenseInterface[]) => {
      this.dataService.setExpenses(expenses);
    });
  }

}
