import { ExpenseInterface } from './../../interface/expenseinterface';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private readonly _expenses: BehaviorSubject<ExpenseInterface[]>;

  
  constructor() { 
    this._expenses = new BehaviorSubject<ExpenseInterface[]>(null);
  }

  async getExpenses(): Promise<ExpenseInterface[]> {
    return this._expenses.getValue();
  }

  async setExpenses(expenses: ExpenseInterface[]): Promise<any> {
    return this._expenses.next(expenses);
  }

  getExpenseSubscription(): BehaviorSubject<ExpenseInterface[]> {
    return this._expenses;
  }

}
