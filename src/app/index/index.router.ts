import { IndexPage } from './index.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: IndexPage,
    children: [
      {
        path: '',
        loadChildren: () => 
        import('../pages/dashboard/dashboard.module').then(
            m => m.DashboardPageModule)
      },
      {
        path: 'settings',
        loadChildren: () => 
        import('../pages/settings/settings.module').then(
            m => m.SettingsPageModule)
      }
      
    ]
  },
  {
    path: 'dashboard',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IndexRouter {}
