import { DatetimeService } from './../../services/datetime/datetime.service';
import { ExpenseInterface } from './../../interface/expenseinterface';
import { DataService } from './../../services/data/data.service';
import { AddExpenseComponent } from './../../components/add-expense/add-expense.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SubscriptionLike, BehaviorSubject } from 'rxjs';
import { ActionService } from '../../services/action/action.service';
import { ENGINE_METHOD_DIGESTS } from 'constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  expenses: ExpenseInterface[];
  subscription: SubscriptionLike;
 // todayDate: Date;
  installDate: Date;
  selectedDate: Date;
  dateSubscription: SubscriptionLike;

  constructor(
    private modalController: ModalController,
    private dataService: DataService,
    private actionService: ActionService,
    private dataTimeService: DatetimeService
  ) { 
    // this.expenses = [];
    //this.actionService.getTodaysExpensesFromLocal().then((value => this.expenses = value));
    this.actionService.getTodaysExpensesFromLocal().then((expenses => this.expenses = expenses));

    // this.selectedDate = this.dataTimeService.selectedDate;
    this.installDate = this.dataTimeService.installDate;
    // console.log(this.todayDate);
    // console.log(this.installDate);
    // this.selectedDate = this.dataTimeService.selectedDate;
    //this.todayDate = this.dataTimeService.getCurrentDateTime();
  }

  ngOnInit() {
    this.dateSubscription = this.dataTimeService.getSelectedDateSubscription()
      .subscribe({
        next: (date: Date) => {
          this.selectedDate =date;
        },
        error: (err) => {
          console.log(err)
        },
        complete: () => {

        }
      })
    //this.selectedDate = this.dataTimeService.getCurrentDateTime();
    this.subscription = this.dataService.getExpenseSubscription()
    .subscribe({
        next: (expense: ExpenseInterface[] ) => {
          if(expense != null ){
           // this.expenses.push(expense);
           this.expenses = expense;
          } else {
            this.expenses = [];
          }
        },
        error: (err) => {
          console.log(err);
        },
        compelete: () => {}
      });


  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddExpenseComponent
    });
    return await modal.present();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    
  }

  changeSelectedDate(value: string): void {
    //this.selectedDate = this.dataTimeService.createDateFromString(value);
    // console.log(typeof this.selectedDate);
    // console.log(this.selectedDate);
   // this.dataTimeService.selectedDate = this.dataTimeService.createDateFromString(value);
    this.selectedDate = this.dataTimeService.createDateFromString(value);
    this.dataTimeService.setSelectedDate(value).then(() => {
      this.actionService.emitExpensesByDateFromLocal(this.selectedDate);
    });
  }

  setCurrentToTodayDate(): void{
    // this.todayDate = this.dataTimeService.getCurrentDateTime();
    //this.selectedDate = this.dataTimeService.getCurrentDateTime();
    // this.dataTimeService.setSelectedDate(this.dataTimeService.getCurrentDateTime());
      //this.selectedDate = this.todayDate;
    this.dataTimeService.setSelectedDate(this.dataTimeService.getCurrentDateTime()).then(() => {
      this.actionService.emitExpensesByDateFromLocal(this.selectedDate);
    });
  }
  
}
